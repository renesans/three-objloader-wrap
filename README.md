# three-objloader-wrap

THREE.js ObjLoader wrapper for npms module

With yarn
```bash
yarn add three-objloader-wrap
```

With npm
```bash
npm i three-objloader-wrap
```

Usage

```es6
    import * as THREE from 'three';
    import {ObjLoaderWrap} from "three-objloader-wrap";

    ObjLoaderWrap(THREE);

    // now use THREE.ObjLoader
```